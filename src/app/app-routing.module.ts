import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
    { path: '', loadChildren: () => import('./mission/mission.module').then(m => m.MissionModule) },
    { path: 'member', loadChildren: () => import('./member/member.module').then(m => m.MemberModule) },
    { path: '', loadChildren: () => import('./team/team.module').then(m => m.TeamModule) },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
  
export class AppRoutingModule { }
