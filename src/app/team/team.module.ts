import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamComponent } from './components/team/team.component';
import { TeamRoutingModule } from './team-routing.module';
import { TeamService } from './services/team.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormComponent } from './components/form/form.component';


// import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TeamComponent,
    FormComponent,
  ],
  imports: [
    CommonModule,
    TeamRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    // SharedModule,
    // FormsModule, 
  ],
  providers:[
    TeamService
  ],
  exports: [RouterModule]
})
export class TeamModule { }
