import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Team } from '../../model/team';
import { TeamService } from '../../services/team.service';
import { Member } from '../../../member/models/member';
import { MemberService } from '../../../member/services/member.service';
import { MissionService } from 'src/app/mission/services/mission.service';
import { Mission } from 'src/app/mission/models/mission.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})


export class FormComponent implements OnInit {
  teamForm!: FormGroup;
  members: Member[] = [];
  missions: Mission[] = [];
  
 
  constructor(private formBuilder: FormBuilder, private teamService: TeamService, private memberService: MemberService
    , private missionService: MissionService
    ) { }

  ngOnInit(): void {

    this.initForm();
    this.memberService.getMembers().subscribe((members:any) => {
      this.members = members;
    });
    this.missionService.getMissions().subscribe((missions:any) => {
      this.missions = missions;
    });

    
    
    
  }

  private initForm(): void {
    this.teamForm = this.formBuilder.group({
      name: ['', Validators.required],
      members: [[]], 
      missions: [[]] 
    });
  }

  onSubmit(): void {
    
  // Envoyer l'objet newTeam à votre service ou effectuer toute autre opération requise
  console.log('newTeam', this.teamForm.value);

    // if (this.teamForm.valid) {
    //   const newTeam: Team = {
    //     id: 0,
    //     name: this.teamForm.value.name,
    //     members: this.teamForm.value.members, 
    //     missions:this.teamForm.value.missions
    //   };
    //   console.log('newTeam',newTeam)
    // }
  }
}
