import { Component } from '@angular/core';
import { TeamService } from '../../services/team.service';
import { Router } from '@angular/router';
import { Team } from '../../model/team';
import { MemberService } from 'src/app/member/services/member.service';
import { Member } from 'src/app/member/models/member';
import { MissionService } from 'src/app/mission/services/mission.service';
import { Mission } from 'src/app/mission/models/mission.model';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent {

  constructor(private teamService: TeamService,private router: Router,
    private memberService: MemberService,
    private missionService: MissionService,
    ) { }
  teams: Team[] = [];
  members: Member[] = [];
  missions: Mission[] = [];
  ngOnInit(): void {
    this.teamService.getTeams().subscribe((res:any) => {
      this.teams = res;
    });
    this.memberService.getMembers().subscribe((res:any)=>{
      this.members= res;
    })
    this.missionService.getMissions().subscribe((res:any)=>{
      this.missions= res;
    })
  }
  getMemberName(memberId: any)  {
    if (!this.members) return ''; 
    const member = this.members.find((member: any) => member.id == memberId);
    return member ? member.name : '';
  }
  getMissionName(missionId:any){
    if (!this.missions) return ''; 
    const mission = this.missions.find((mission: any) => mission.id == missionId);
    return mission ? mission.title : '';
  }

  addTeam(){
    this.router.navigate(['/add-team']); 
  }
  deleteTeam(id:any){
    console.log('id',id);
  }
  editTeam(id:any){
    console.log(id)
  }

}
