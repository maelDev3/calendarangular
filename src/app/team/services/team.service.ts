// team.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Team } from '../model/team';
import { Mission } from '../../mission/models/mission.model';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private teamsUrl = 'http://localhost:3000/teams';
  private missionUrl = 'http://localhost:3000/missions';
  constructor(private http: HttpClient) {}

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamsUrl);
  }

  getTeam(id: number): Observable<Team> {
    const url = `${this.teamsUrl}/${id}`;
    return this.http.get<Team>(url);
  }

  addTeam(team: Team): Observable<Team> {
    return this.http.post<Team>(this.teamsUrl, team);
  }

  updateTeam(team: Team): Observable<any> {
    return this.http.put(`${this.teamsUrl}/${team.id}`, team);
  }

  deleteTeam(id: number): Observable<any> {
    const url = `${this.teamsUrl}/${id}`;
    return this.http.delete(url);
  }

  
  getMissionById(missionId:any): Observable<Mission> {
    return this.http.get<Mission>(`${this.missionUrl}/${missionId}`);
  }
}
