


import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FormComponent } from './components/form/form.component';
import { TeamComponent } from './components/team/team.component';

const routes: Routes = [
  
  { path: 'team', component: TeamComponent },
  { path: 'add-team', component: FormComponent },
  { path: '', redirectTo: '/team', pathMatch: 'full' } 

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
