import { Member } from '../../member/models/member';
import { Mission } from 'src/app/mission/models/mission.model';
export interface Team {
    id: number;
    name: string;
    members: Member[];
    missions: Mission[];
  }
  