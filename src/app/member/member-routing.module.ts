import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormMemberComponent } from './components/form-member/form-member.component';
import { MemberComponent } from './components/member/member.component';

const routes: Routes = [
  { path: 'add', component: FormMemberComponent },
  { path: '', component: MemberComponent },
  { path: '', redirectTo: '/member', pathMatch: 'full' } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
