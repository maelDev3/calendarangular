// member.model.ts
export interface Member {
    id: number;
    name: string;
    photoUrl: string;
  }