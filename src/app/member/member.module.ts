import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemberComponent } from './components/member/member.component';
import { MemberRoutingModule } from './member-routing.module';
import { MemberService } from './services/member.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormMemberComponent } from './components/form-member/form-member.component';

import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    MemberComponent,
    FormMemberComponent,
  ],
  imports: [
    CommonModule,
    MemberRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers:[
    MemberService
  ],
  exports: [RouterModule]
})
export class MemberModule { }
