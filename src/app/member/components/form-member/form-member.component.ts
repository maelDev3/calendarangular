import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Member } from '../../models/member';
import { MemberService } from '../../services/member.service';
import { Router } from '@angular/router';
import { ImageService } from '../../services/image.service';

@Component({
  selector: 'app-form-member',
  templateUrl: './form-member.component.html',
  styleUrls: ['./form-member.component.css']
})
export class FormMemberComponent {
  memberForm!: FormGroup;
  selectedFile: string | ArrayBuffer | null = null;

  constructor(private imageService: ImageService, private formBuilder: FormBuilder, private memberService: MemberService,private router: Router) { }

  Form: Member = { id: 0, name: '', photoUrl: ''};

  ngOnInit(): void {
    this.initForm();
    this.getHighestMemberId();
  }

  private initForm(): void {
    this.memberForm = this.formBuilder.group({
      name: ['', Validators.required],
      photo: [null, Validators.required] // FormControl pour le fichier
    });
  }

  

  getHighestMemberId(): void {
    this.memberService.getMembers().subscribe(
      (members:any) => {
        if (members.length > 0) {
          const maxId = Math.max(...members.map((member:any) => member.id));
          this.Form.id = maxId + 1;
        } else {
          this.Form.id = 1; // Si aucun élément dans le tableau, commencer par l'ID 1
        }
      },
      (error:any) => {
        console.error('Erreur lors de la récupération des members:', error);
      }
    );
  }

  resetNewMission(): void {
    this.Form = { id: 0, name: '', photoUrl: '' };
    this.getHighestMemberId(); 
  }

 
  
  onSubmit(): void {
    if (this.memberForm.valid) {
      const formValue = this.memberForm.value;
      const newMember: Member = { id:0,name: formValue.name, photoUrl: formValue.photo };
      this.memberService.addMember(newMember).subscribe(() => {
        console.log('Member added successfully.');
        // Réinitialiser le formulaire après l'ajout
        this.memberForm.reset();
        this.selectedFile = null; // Réinitialiser l'image sélectionnée
        this.router.navigate(['/member']);
      });
    }
  }

  

  onFileSelected(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        // Stockez l'image dans le dossier assets/member
        const imageName = `member_${this.Form.id}.${file.type.split('/')[1]}`;
        const imagePath = `assets/member/${imageName}`;
  
        // Mettez à jour la propriété photoUrl avec le chemin relatif de l'image
        this.Form.photoUrl = imagePath;
  
        // Mettez à jour l'aperçu de l'image sélectionnée
        this.selectedFile = reader.result;
  
      };
    }
  }
  

  
}
