import { Component } from '@angular/core';

import { Member } from '../../models/member';
import { MemberService } from '../../services/member.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent {
  constructor(private memberService: MemberService,private router: Router) { }
  members: Member[] = [];

  ngOnInit(): void {
    this.memberService.getMembers().subscribe((members:any) => {
      console.log(members);
      this.members = members;
    });
  }
  addMember(){
    this.router.navigate(['/member/add']); // Assurez-vous que '/missions' est le chemin de votre liste de missions
  }

}
