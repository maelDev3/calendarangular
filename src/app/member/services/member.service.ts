// member.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Member } from '../models/member';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  private membersUrl = 'http://localhost:3000/members';

  constructor(private http: HttpClient) {}

  getMembers(): Observable<Member[]> {
    return this.http.get<Member[]>(this.membersUrl);
  }

  getMember(id: number): Observable<Member> {
    const url = `${this.membersUrl}/${id}`;
    return this.http.get<Member>(url);
  }

  addMember(member: Member): Observable<Member> {
    return this.http.post<Member>(this.membersUrl, member);
  }

  updateMember(member: Member): Observable<any> {
    return this.http.put(`${this.membersUrl}/${member.id}`, member);
  }

  deleteMember(id: number): Observable<any> {
    const url = `${this.membersUrl}/${id}`;
    return this.http.delete(url);
  }
}
