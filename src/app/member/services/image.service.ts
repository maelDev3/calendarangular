import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  constructor(private http: HttpClient) {}

  // Méthode pour enregistrer l'image sur le serveur ou dans un service de stockage approprié
  saveImage(image: File): Observable<string> {
    const formData = new FormData();
    formData.append('file', image);

    return this.http.post<string>('http://example.com/upload', formData);
  }
}
