import { Component } from '@angular/core';
import { CalendarOptions, EventInput } from '@fullcalendar/core';
import { TeamService } from '../team/services/team.service';
import { Team } from '../team/model/team';
import { Mission } from '../mission/models/mission.model';
import { Observable, forkJoin } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import dayGridPlugin from '@fullcalendar/daygrid';
import { Router } from '@angular/router';
import { Member } from '../member/models/member'; // Import Member model
import { MemberService } from '../member/services/member.service';
import * as bootstrap from 'bootstrap';



@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
})
export class CalendarComponent {
  calendarOptions: CalendarOptions = {
    plugins: [dayGridPlugin],
    initialView: 'dayGridMonth',
    weekends: false,
    events: [] as EventInput[], 
    eventContent: this.renderEventContent.bind(this), // Bind event rendering function
    eventClick: this.handleEventClick.bind(this), // Bind event click handler
    // eventMouseEnter: this.handleEventMouseEnter.bind(this) 

  };
  members: Member[] = []; 
  
  currentDescriptionElement: HTMLElement | null = null;
  constructor(private router: Router, private teamService: TeamService, private memberService: MemberService) {
    // this.calendarOptions.eventMouseEnter = this.handleEventMouseEnter.bind(this);
    // this.calendarOptions.eventMouseLeave = this.handleEventMouseLeave.bind(this);
  }

  ngOnInit() {
    this.loadMissions();
    this.loadMembers(); // Load members data
  }

  loadMissions() {
    this.teamService.getTeams().pipe(
      mergeMap((teams: Team[]) => {
        const observables: Observable<any>[] = [];
        teams.forEach((team: Team) => {
          if (Array.isArray(team.missions)) { // Check if team.missions is an array
            team.missions.forEach((missionId: any) => {
              observables.push(this.teamService.getMissionById(missionId).pipe(map((mission: Mission | undefined) => {
                  if (mission !== undefined) {
                    const filteredMembers = team.members.reduce((filtered: any[], memberId: any) => {
                      const member = this.members.find((m: Member) => m.id == memberId);
                      if (member) {
                          filtered.push({
                              id: member.id,
                              name: member.name,
                              photo: member.photoUrl ? member.photoUrl : 'path/to/default/photo.jpg'
                          });
                      }
                      return filtered;
                    }, []);

                    const formattedEndDate = new Date(mission.endDate).toISOString();

                    return {
                      id: String(mission.id), 
                      title: mission.title,
                      start: mission.startDate,
                      end: mission.endDate,
                      description: mission.description,
                      color: mission.color,
                      team: team.name,
                      members: filteredMembers
                    } as EventInput;
                  } else {
                    return null;
                  }
                })
              ));
            });
          }
        });
        return forkJoin(observables);
      })
    ).subscribe((results: (EventInput | null)[]) => {
      const events: EventInput[] = [];
      results.forEach((result: EventInput | null) => {
        if (result !== null) {
          events.push(result);
        }
      });
      this.calendarOptions.events = events;
    });
  }



 

  
  

  

  
  renderEventContent(arg: any) {
    const event = arg.event;
    const teamName = event.extendedProps.team; 
    const members = event.extendedProps.members; 
    let content = '<div class="fc-content" >';
    content += '<h5 class="fc-title">' + event.title + '</h5>'; 
    content += '<h6 class="fc-team">' + teamName + '</h6>'; 

    if (members && members.length > 0) {
      content += '<div class="fc-members row">';
      members.forEach((member: any) => {
        content += '<div class="col-6 col-md-3">';
        content += '<div class="fc-member text-center mb-3">';
        // Utiliser les classes de taille d'image Bootstrap (par exemple, "img-thumbnail") pour définir la taille de l'image
        content += '<img class="fc-member-photo img-fluid rounded-circle img-thumbnail" width="60px" height="60px" src="' + member.photo + '" alt="' + member.name + '">';
        content += '<div class="fc-member-name">' + member.name + '</div>';
        content += '</div>';
        content += '</div>';
      });
      content += '</div>';
    }
    content += '</div>';
    return { html: content };
  }

  handleEventMouseEnter(arg: any) {
    const event = arg.event;
    const description = event.extendedProps.description; // Récupérez la description de l'événement
    
      if (description) {
        console.log(description);
        // Vérifiez si une description est déjà affichée
        if (this.currentDescriptionElement) {
            // Supprimez l'élément de la description actuellement affiché
            document.body.removeChild(this.currentDescriptionElement);
            this.currentDescriptionElement = null;
        }
        // Créez un élément pour afficher la nouvelle description
        const descriptionElement = document.createElement('div');
        descriptionElement.className = 'custom-description';
        descriptionElement.textContent = description;
         // Ajouter des styles à l'élément de description
          descriptionElement.style.position = 'absolute';
          descriptionElement.style.top = '100px';
          descriptionElement.style.left = '343px';
    
        // Ajoutez le nouvel élément au corps du document
        document.body.appendChild(descriptionElement);
        this.currentDescriptionElement = descriptionElement;
    
        // Obtenez les coordonnées de la mission dans le calendrier
        const missionRect = event.el.getBoundingClientRect();
    
        // Mettez à jour la position de l'élément de description en fonction des coordonnées de la mission
        descriptionElement.style.top = missionRect.top + window.scrollY + 'px';
        descriptionElement.style.left = missionRect.left + window.scrollX + 'px';
    }
       
    
      
  }

  

 
  loadMembers() {
    this.memberService.getMembers().subscribe((res:any)=>{
      this.members= res;
    });
  }

  
  loadTeam() {
    this.teamService.getTeams().subscribe((res:any)=>{
      // this.team= res;
    });
  }

  getMember(memberId: any): Member | undefined {
    return this.members.find(member => member.id == memberId);
  }

  handleEventClick(arg: any) {
    const clickedEvent = arg.event;
    // Supprimer l'élément de description s'il est affiché
    if (this.currentDescriptionElement) {
      document.body.removeChild(this.currentDescriptionElement);
      this.currentDescriptionElement = null;
  }
    this.router.navigate(['/edit-mission', clickedEvent.id]);
  }
  

}


