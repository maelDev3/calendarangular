import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MissionListComponent } from './components/mission-list/mission-list.component';
import { MissionRoutingModule } from './mission-routing.module';
import { MissionService } from './services/mission.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { MissionFormComponent } from './components/mission-form/mission-form.component';
import { MissionFormModule } from './components/mission-form/mission-form.module';
import { EditMissionComponent } from './components/edit-mission/edit-mission.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MissionListComponent,
    EditMissionComponent
    
  ],
  imports: [
    CommonModule,
    MissionRoutingModule,
    HttpClientModule,
    MissionFormModule,
    FormsModule,   
  ],
  providers:[
      MissionService
  ],
  exports: [RouterModule]
})
export class MissionModule { }
