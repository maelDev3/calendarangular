


import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MissionListComponent } from './components/mission-list/mission-list.component';
import { CalendarComponent } from '../calendar/calendar.component';
import { MissionFormComponent } from './components/mission-form/mission-form.component';
import { EditMissionComponent } from './components/edit-mission/edit-mission.component';

const routes: Routes = [
  { path: 'calendar', component: CalendarComponent },
  { path: 'missions', component: MissionListComponent },
  { path: 'add-mission', component: MissionFormComponent },
  { path: 'edit-mission/:id', component: EditMissionComponent },
  { path: '', redirectTo: '/calendar', pathMatch: 'full' } // Redirection par défaut vers le calendrier

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MissionRoutingModule { }
