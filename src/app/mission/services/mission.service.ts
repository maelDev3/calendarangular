import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { Mission } from '../models/mission.model';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  private apiUrl = 'http://localhost:3000/missions';

  constructor(private http: HttpClient) { }

  getMissions(): Observable<Mission[]> {
    return this.http.get<Mission[]>(this.apiUrl);
  }

  getMissionById(id: number): Observable<Mission> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Mission>(url);
  }

  addMission(newMission: any): Observable<Mission> {
   newMission.id = String(newMission.id);

    return this.http.post<Mission>(this.apiUrl, newMission);
  }


  updateMission(updatedMission: Mission): Observable<Mission> {
    const url = `${this.apiUrl}/${updatedMission.id}`;
    return this.http.put<Mission>(url, updatedMission);
  }

 
  deleteMission(missionId: any): Observable<Mission> {
    const url = `${this.apiUrl}/${missionId}`;
    return this.http.delete<Mission>(url)
      .pipe(
        catchError((error: any) => {
          console.error('Error deleting mission:', error);
          return throwError('Failed to delete mission');
        })
      );
  }

  removeMissionLocally(missionId: number, missions: Mission[]): Mission[] {
    // Recherche de l'indice de l'élément à supprimer dans le tableau
    const index = missions.findIndex(mission => mission.id === missionId);
    if (index !== -1) {
      // Suppression de l'élément du tableau localement
      missions.splice(index, 1);
    }
    return missions;
  }


}
