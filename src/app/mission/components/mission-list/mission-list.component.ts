import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Mission } from '../../models/mission.model';
import { MissionService } from '../../services/mission.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-mission-list',
  templateUrl: './mission-list.component.html',
})
export class MissionListComponent implements OnInit, AfterViewInit {
  missions!: Mission[];
  missionIdToDelete: any = null;
  @ViewChild('confirmDeleteModal') confirmDeleteModal!: ElementRef;


  constructor(private missionService: MissionService, private router: Router) { }

  ngOnInit(): void {
    this.loadMissions();
  }

  ngAfterViewInit(): void {
    // Assurez-vous que la référence de vue est initialisée avant d'accéder à nativeElement
    if (this.confirmDeleteModal) {
      // Vous pouvez afficher le modal au chargement de la page ici
      // this.confirmDeleteModal.nativeElement.showModal();
    }
  }

  loadMissions() {
    this.missionService.getMissions().subscribe((missions: any) => {
      
      this.missions = missions;
    });
  }

  addMission() {
    this.router.navigate(['/add-mission']);
  }

  confirmDelete(missionId: any) {
   
    const confirmed = window.confirm('Êtes-vous sûr de vouloir supprimer cette mission ?');
  if (confirmed) {
    // Si l'utilisateur confirme, appelez la méthode deleteConfirmed()
    this.deleteConfirmed(missionId);
  } else {
    // Si l'utilisateur annule, ne faites rien
    return;
  }
 
  }

  deleteConfirmed(missionId:number) {
    
    // Supprimer la mission localement après la suppression réussie sur le serveur
    // console.log('missionId',missionId);
    
      this.missionService.deleteMission(missionId).subscribe(() => {
        this.missions = this.missionService.removeMissionLocally(missionId, this.missions);
      });

  }

  editMission(missionId: any) {
    this.router.navigate(['/edit-mission', missionId]);
  }
}
