import { Component } from '@angular/core';
import { MissionService } from '../../services/mission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Mission } from '../../models/mission.model';
import { Team } from 'src/app/team/model/team';
import { TeamService } from 'src/app/team/services/team.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-mission',
  templateUrl: './edit-mission.component.html',
  styleUrls: ['./edit-mission.component.css']
})
export class EditMissionComponent {
  newMission: Mission = { id: 0, title: '', description: '', color: '', teamId:0, startDate: '', endDate: '' };
  teams: Team[] = [];
  
  constructor(
    private missionService: MissionService,
    private router: Router,
    private route: ActivatedRoute,
    private teamsService:TeamService
  ) {}

  ngOnInit(): void {
    const missionIdParam = this.route.snapshot.paramMap.get('id');
    const missionId = missionIdParam ? parseInt(missionIdParam, 10) : undefined;
    if (missionId !== undefined) {
      this.getMissionToEdit(missionId);
    } 
    this.teamsService.getTeams().subscribe((teams:any) => {
      console.log(teams);
      this.teams = teams;
    });
  }

  dateValidator(form: FormGroup) {
    const startDate = new Date(form.get('startDate')!.value);
    const endDate = new Date(form.get('endDate')!.value);

    if (startDate > endDate) {
      form.get('endDate')!.setErrors({ endDateBeforeStartDate: true });
    } else {
      form.get('endDate')!.setErrors(null);
    }
  }

  getMissionToEdit(id: number): void {
    this.missionService.getMissionById(id).subscribe(
      (mission: Mission) => {
        console.log('mission',mission);
        this.newMission = mission; // Initialiser newMission avec les données récupérées
      },
      (error: any) => {
        console.error('Erreur lors de la récupération de la mission à éditer:', error);
      }
    );
  }

  onSubmit() {
    // if (typeof this.newMission.teamId === 'string') {
    //   this.newMission.teamId = parseInt(this.newMission.teamId, 10);
    // }
 
    this.missionService.updateMission(this.newMission).subscribe(
      (updatedMission: Mission) => {
        console.log('Mission mise à jour avec succès:', updatedMission);
        this.router.navigate(['/calendar']); // Rediriger vers la liste des missions après la mise à jour
      },
      (error: any) => {
        console.error('Erreur lors de la mise à jour de la mission:', error);
      }
    );
  }
}
