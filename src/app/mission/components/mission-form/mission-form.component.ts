import { Component, EventEmitter, Output } from '@angular/core';
import { Mission } from '../../models/mission.model';
import { MissionService } from '../../services/mission.service';
import { Router } from '@angular/router';
import { TeamService } from 'src/app/team/services/team.service';
import { Team } from '../../../team/model/team';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-mission-form',
  templateUrl: './mission-form.component.html',
  // styleUrls: ['./mission-form.component.scss']
})
export class MissionFormComponent {

  constructor(private missionService: MissionService,private router: Router,
    private teamsService:TeamService,
    private formBuilder: FormBuilder
    ) {

    }

  @Output() addMission = new EventEmitter<Mission>();
  

  newMission: Mission = { id: 0, title: '', description: '', color: '', teamId:"" ,startDate:"",endDate:""};
  
  teams: Team[] = [];
  ngOnInit(): void {
    this.getHighestMissionId();

    this.teamsService.getTeams().subscribe((teams:any) => {
      console.log(teams);
      this.teams = teams;
    });

    
    
  }

  dateValidator(form: FormGroup) {
    const startDate = new Date(form.get('startDate')!.value);
    const endDate = new Date(form.get('endDate')!.value);

    if (startDate > endDate) {
      form.get('endDate')!.setErrors({ endDateBeforeStartDate: true });
    } else {
      form.get('endDate')!.setErrors(null);
    }
  }

  

  getHighestMissionId(): void {
    this.missionService.getMissions().subscribe(
      (missions:any) => {
        if (missions.length > 0) {
          const maxId = Math.max(...missions.map((mission:any) => mission.id));
          this.newMission.id = maxId + 1;
        } else {
          this.newMission.id = 1; // Si aucun élément dans le tableau, commencer par l'ID 1
        }
      },
      (error:any) => {
        console.error('Erreur lors de la récupération des missions:', error);
      }
    );
  }

  resetNewMission(): void {
    this.newMission = { id: 0, title: '', description: '',teamId:'', color: '',startDate:'',endDate:"" };
    this.getHighestMissionId(); // Récupérer le nouvel ID après l'ajout de la mission
  }

  

  onSubmit() {

    // Récupérer les valeurs des champs de date
  const startDate = new Date(this.newMission.startDate);
  const endDate = new Date(this.newMission.endDate);
  if (startDate > endDate) {
    alert('La date de début doit être antérieure à la date de fin.');
    console.error('La date de début doit être antérieure à la date de fin.');
    return; // Empêcher la soumission du formulaire
  }
    console.log('this.newMission',this.newMission);
    if(this.newMission.title && this.newMission.description && this.newMission.color && this.newMission.startDate && this.newMission.endDate){
      this.missionService.addMission(this.newMission).subscribe((addedMission:any) => {
        console.log('Nouvelle mission ajoutée avec succès:', addedMission);
        if(addedMission.id){
          this.router.navigate(['/calendar']); // Assurez-vous que '/missions' est le chemin de votre liste de missions
        }
      },
      (error:any) => {
        console.error('Erreur lors de l\'ajout de la mission:', error);
      }
    );
    }
   
    this.newMission = { id: 0, title: '', description: '', color: '',teamId:"" ,startDate:'',endDate:""  };
 
  }
}
