import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { MissionFormComponent } from './mission-form.component';
import { FormsModule } from '@angular/forms'; // Importez FormsModule
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MissionFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[
    
  ],
  exports: [RouterModule]
})
export class MissionFormModule  { }
