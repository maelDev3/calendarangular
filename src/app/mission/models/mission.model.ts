export interface Mission {
    id: number;
    title: string;
    description: string;
    color: string;
    teamId?: any; // Utilisation d'un point d'interrogation pour déclarer teamId comme optionnel
    startDate: string;
    endDate: string;
  }
  