const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

function minifyImages() {
  return gulp.src('src/assets/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/assets/images'));
}

exports.minifyImages = minifyImages;
